import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: root

    property var creationData: null

    title: qsTr("Create new album")
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    GridLayout {
        id: internal
        columns: 2

        property var privacyModel: [
            { "name": qsTr("Nobody"), "value": "SELF" },
            { "name": qsTr("Everybody"), "value": "EVERYONE" },
            { "name": qsTr("Friends only"), "value": "ALL_FRIENDS" },
            { "name": qsTr("Friends and friends of friends"), "value": "FRIENDS_OF_FRIENDS" }
        ]

        Label {
            text: qsTr("Album name:")
        }

        TextField {
            id: titleField
            Layout.fillWidth: true
        }

        Label {
            text: qsTr("Description:")
        }

        TextArea {
            id: descriptionField
            Layout.fillWidth: true
        }

        Label {
            text: qsTr("View permissions:")
        }

        ComboBox {
            id: viewPrivacyField
            Layout.fillWidth: true
            model: internal.privacyModel
            textRole: "name"
        }
    }

    onAccepted: {
        // TODO: validate!
        var data = {
            "name": titleField.text,
            "message": descriptionField.text,
            "privacy": getPrivacy(viewPrivacyField),
        }
        root.creationData = data
    }

    function getPrivacy(field) {
        return "{\"value\":\"" + field.model[field.currentIndex].value + "\"}"
    }
}
