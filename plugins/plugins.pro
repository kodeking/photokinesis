TEMPLATE = subdirs
SUBDIRS = \
    deviantart \
    facebook \
    flickr \
    ftp \
    google \
    vk

contains(EXTRA_PLUGINS, "instagram") {
    SUBDIRS += instagram
}
