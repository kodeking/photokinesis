import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: root

    property var fileProvider: null

    title: qsTr("Remote directories")
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    property string _initialPath: ""

    Component.onCompleted: _initialPath = uploader.path

    onRejected: uploader.path = _initialPath

    ColumnLayout {
        id: internal
        anchors.fill: parent

        Label {
            text: qsTr("Current directory:")
        }

        Rectangle {
            Layout.fillWidth: true
            color: palette.base
            border { color: palette.shadow; width: 1 }
            implicitHeight: dirLabel.implicitHeight + 4
            implicitWidth: dirLabel.implicitWidth
            Label {
                id: dirLabel
                anchors { fill: parent; margins: 2 }
                text: uploader.path
                horizontalAlignment: Text.AlignHCenter
                elide: Text.ElideLeft
                font.pixelSize: 16
            }
        }

        ToolBar {
            Layout.fillWidth: true
            RowLayout {
                anchors.fill: parent
                ToolButton {
                    iconSource: "qrc:/ftp/create-dir.svg"
                    tooltip: qsTr("Create a new directory")
                    text: qsTr("New folder...")
                    onClicked: PopupUtils.open(Qt.resolvedUrl("NewFolderDialog.qml"), root, {
                        "fileProvider": uploader
                    })
                }
            }
        }

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle {
                anchors.fill: parent
                color: palette.base
                border { color: palette.shadow; width: 1 }
            }

            ListView {
                anchors { fill: parent; margins: 2 }
                model: uploader.subdirs
                delegate: Label {
                    width: parent.width
                    text: qsTr("\uD83D\uDCC1 %1").arg(modelData)
                    MouseArea {
                        anchors.fill: parent
                        onClicked: uploader.cd(modelData)
                    }
                }
            }
        }
    }

    SystemPalette { id: palette; colorGroup: SystemPalette.Active } 
}
