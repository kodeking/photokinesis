/*
 * Copyright (C) 2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "scp_backend.h"

#include <QDebug>

using namespace Photokinesis;
using namespace QSsh;

ScpBackend::ScpBackend():
    m_currentDir(".")
{
}

ScpBackend::~ScpBackend()
{
}

static FtpInterface::Error mapError(QSsh::SshError sshError) {
    switch (sshError) {
    case QSsh::SshAuthenticationError:
        return FtpInterface::AuthenticationError;
    case QSsh::SshTimeoutError:
        return FtpInterface::ServerTimeout;
    default:
        return FtpInterface::ConnectionRefused;
    }
}

void ScpBackend::login(const QString &host,
                       uint16_t port,
                       const QString &username,
                       const QString &password)
{
    SshConnectionParameters params;
    params.setHost(host);
    params.setPort(port > 0 ? port : 22);
    params.setUserName(username);
    params.setPassword(password);
    params.authenticationType = SshConnectionParameters::AuthenticationTypePassword;
    params.timeout = 10;
    m_connection.reset(new SshConnection(params));

    QObject::connect(m_connection.data(), &SshConnection::error,
                     this, [this](QSsh::SshError error) {
        qDebug() << "Error" << error << m_connection->errorString();
        Q_EMIT stateChanged(state(), mapError(error));
    });
    QObject::connect(m_connection.data(), &SshConnection::connected,
                     this, &ScpBackend::onConnected);
    QObject::connect(m_connection.data(), &SshConnection::disconnected,
                     this, [this]() {
        Q_EMIT stateChanged(Unconnected);
    });

    Q_EMIT stateChanged(Connecting);
    m_connection->connectToHost();
}

void ScpBackend::mkdir(const QString &dirName)
{
    auto job = m_channel->createDirectory(m_currentDir + "/" + dirName);
    onFinished(job, [this](const QString &error) {
        qDebug() << "Dir created:" << error;
        Q_EMIT dirCreated(error.isEmpty());
    });
}

void ScpBackend::upload(const QString &localFile, const QString &fileName)
{
    auto job = m_channel->uploadFile(localFile,
                                     m_currentDir + "/" + fileName,
                                     SftpOverwriteExisting);
    onProgress(job, [this](quint64 uploaded, quint64 total) {
        qDebug() << "Progress" << uploaded << total;
        Q_EMIT progress(uploaded, total);
    });
    onFinished(job, [this](const QString &error) {
        qDebug() << "Upload finished:" << error;
        Q_EMIT finished(error);
    });
}

void ScpBackend::logout()
{
    m_connection->disconnectFromHost();
}

void ScpBackend::cd(const QString &dirName)
{
    QString newDir = m_currentDir + "/" + dirName;
    auto job = m_channel->listDirectory(newDir);
    onListAvailable(job, [this, newDir](const QStringList &files) {
        qDebug() << "List available:" << files;
        m_currentDir = newDir;
        Q_EMIT subdirsChanged(files);
    });
}

FtpInterface::State ScpBackend::state() const
{
    switch (m_connection->state()) {
    case SshConnection::Connected:
        return Connected;
    case SshConnection::Connecting:
        return Connecting;
    default:
        return Unconnected;
    }
}

void ScpBackend::onConnected()
{
    m_channel = m_connection->createSftpChannel();

    m_channel->initialize();
    QObject::connect(m_channel.data(), &SftpChannel::initialized,
                     this, [this]() {
        Q_EMIT stateChanged(Connected);
    });
    QObject::connect(m_channel.data(), &SftpChannel::channelError,
                     this, [this](const QString &reason) {
        qDebug() << "Initialization failed:" << reason;
        Q_EMIT stateChanged(Unconnected, ConnectionRefused);
    });

    QObject::connect(m_channel.data(), &SftpChannel::finished,
                     this, [this](SftpJobId job, const QString &error) {
        qDebug() << "Job finished:" << error;
        auto i = m_finishedCb.find(job);
        if (i != m_finishedCb.end()) {
            i.value()(error);
            m_finishedCb.erase(i);
        }
        m_listCb.remove(job);
        m_progressCb.remove(job);
    });

    QObject::connect(m_channel.data(), &SftpChannel::fileInfoAvailable,
                     this, [this](SftpJobId job,
                                  const QList<SftpFileInfo> &fileInfoList) {
        QStringList fileList;
        for (const auto &info: fileInfoList) {
            if (info.type == FileTypeDirectory) {
                fileList.append(info.name);
            }
        }
        fileList.sort();
        qDebug() << "File list:" << fileList;
        auto i = m_listCb.find(job);
        if (i != m_listCb.end()) {
            i.value()(fileList);
        }
    });

    QObject::connect(m_channel.data(), &SftpChannel::transferProgress,
                     this, [this](SftpJobId job,
                                  quint64 progress, quint64 total) {
        qDebug() << "Progress:" << progress << total;
        auto i = m_progressCb.find(job);
        if (i != m_progressCb.end()) {
            i.value()(progress, total);
        }
    });
}

void ScpBackend::onFinished(QSsh::SftpJobId id, FinishedCb cb)
{
    m_finishedCb[id] = cb;
}

void ScpBackend::onListAvailable(QSsh::SftpJobId id, ListCb cb)
{
    m_listCb[id] = cb;
}

void ScpBackend::onProgress(QSsh::SftpJobId id, ProgressCb cb)
{
    m_progressCb[id] = cb;
}
