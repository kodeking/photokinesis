import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

GridLayout {
    id: root
    Layout.fillWidth: true
    columns: 2

    Label {
        Layout.minimumWidth: implicitWidth
        text: qsTr("Destination directory:")
    }

    Label {
        Layout.fillWidth: true
        text: uploader.path
        elide: Text.ElideLeft
    }

    Button {
        Layout.columnSpan: 2
        Layout.alignment: Qt.AlignRight
        text: qsTr("Change...")
        onClicked: PopupUtils.open(Qt.resolvedUrl("RemoteFileDialog.qml"), root, {
            "fileProvider": uploader
        })
    }
}
