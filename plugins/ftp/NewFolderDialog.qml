import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: root

    property var fileProvider: null

    title: qsTr("Create Directory")
    standardButtons: StandardButton.NoButton
    /* Unfortunately we can't intercept the Enter key: that always
     * closes the dialog.
     https://bugreports.qt.io/browse/QTBUG-41505
     */
    onAccepted: internal.createDir()

    property bool _done: false

    ColumnLayout {
        id: internal
        anchors.fill: parent

        Label {
            Layout.fillWidth: true
            text: qsTr("Name for the new directory:")
        }

        TextField {
            id: nameField
            Layout.fillWidth: true
            onAccepted: internal.createDir()
        }

        Label {
            id: errorLabel
            color: "red"
            visible: text
        }

        Button {
            Layout.alignment: Qt.AlignRight
            isDefault: true
            text: qsTr("OK")
            onClicked: internal.createDir()
        }

        function createDir() {
            if (root._done) return
            root._done = true // ensure happens just once
            errorLabel.text = ""
            fileProvider.mkdir(nameField.text)
            internal.enabled = false
        }
    }

    Connections {
        target: fileProvider
        onDirCreated: {
            internal.enabled = true
            if (ok) {
                root.close()
            } else {
                errorLabel.text = fileProvider.errorMessage
                nameField.forceActiveFocus()
            }
        }
    }

}
