import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: root

    property var authenticator: null
    property bool wantsVisibility: true
    property string errorMessage: ""

    signal finished()

    anchors { fill: parent; margins: 12 }

    RowLayout {
        Layout.fillWidth: true
        Label {
            text: qsTr("Choose a saved site:")
        }

        ComboBox {
            Layout.fillWidth: true
            model: authenticator.knownUserIds
            onCurrentTextChanged: {
                authenticator.userId = currentText
                authenticator.loadSettings()
            }
        }
    }

    Label {
        Layout.fillHeight: true
        text: qsTr("Or enter the connection details for a new FTP server:")
        font.italic: true
        verticalAlignment: Text.AlignBottom
        wrapMode: Text.WordWrap
    }

    GridLayout {
        Layout.fillWidth: true
        columns: 2
        Label {
            text: qsTr("Host address")
        }

        TextField {
            id: hostnameField
            Layout.fillWidth: true
            placeholderText: qsTr("Host name, or paste a full URL and press Enter")
            text: authenticator.host
            onAccepted: authenticator.tryParseUrl(text)
            onEditingFinished: authenticator.tryParseUrl(text)
        }

        Label {
            text: qsTr("Username")
        }

        TextField {
            id: usernameField
            Layout.fillWidth: true
            placeholderText: qsTr("Your FTP username")
            text: authenticator.username
        }

        Label {
            text: qsTr("Password")
        }

        TextField {
            id: passwordField
            Layout.fillWidth: true
            echoMode: TextInput.Password
            inputMethodHints: Qt.ImhHiddenText
            text: authenticator.password
            onAccepted: root.doLogin()
        }
    }

    Item {
        Layout.columnSpan: 2
        Layout.fillWidth: true
        Layout.fillHeight: true
        implicitHeight: loginButton.implicitHeight + 12
        Button {
            id: loginButton
            anchors {
                top: parent.top; topMargin: 8
                horizontalCenter: parent.horizontalCenter
            }
            text: qsTr("Login")
            onClicked: root.doLogin()
        }
    }

    function doLogin() {
        authenticator.host = hostnameField.text
        authenticator.username = usernameField.text
        authenticator.password = passwordField.text
        authenticator.login()
        root.finished()
    }

    function cancel() {
        root.errorMessage = qsTr("User canceled")
        root.finished()
    }
}
