TARGET = ftp

include(../plugins.pri)

QT += quick

# FTP

INCLUDEPATH += $${OUT_PWD}/qtftp/include
LIBS += -lQt5Ftp
QMAKE_LIBDIR += \
    $${OUT_PWD}/qtftp/lib

# SSH

CONFIG -= no_keywords
INCLUDEPATH += $${PWD}/scp/qssh/src/libs/ssh
LIBS += -lQSsh
QMAKE_LIBDIR += \
    $${OUT_PWD}/scp/qssh/lib

SOURCES += \
    backend_selector.cpp \
    ftp_backend.cpp \
    scp_backend.cpp \
    uploader.cpp

HEADERS += \
    backend_selector.h \
    ftp_backend.h \
    ftp_interface.h \
    scp_backend.h \
    uploader.h

include(scp/scp.pri)
