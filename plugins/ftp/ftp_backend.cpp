/*
 * Copyright (C) 2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ftp_backend.h"

#include <QDebug>
#include <QFile>
#include <QTimer>

using namespace Photokinesis;

FtpBackend::FtpBackend():
    m_subdirsChangedQueued(false)
{
    QObject::connect(&m_ftp, &QFtp::dataTransferProgress,
                     this, &FtpInterface::progress);

    QObject::connect(&m_ftp, &QFtp::stateChanged,
                     [this](int) {
        State state = FtpInterface::Unconnected;
        switch (m_ftp.state()) {
        case QFtp::Unconnected:
            state = FtpInterface::Unconnected; break;
        case QFtp::HostLookup:
        case QFtp::Connecting:
        case QFtp::Connected:
            state = FtpInterface::Connecting; break;
        case QFtp::LoggedIn:
            state = FtpInterface::Connected; break;
        default:
            qWarning() << "QFtp unhandled status" << m_ftp.state();
        }
        Q_EMIT stateChanged(state);
    });

    QObject::connect(&m_ftp, &QFtp::commandFinished,
                     [this](int, bool error) {
        if (error) {
            Q_EMIT finished(m_ftp.errorString());
            if (m_ftp.currentCommand() == QFtp::Login) {
                Q_EMIT stateChanged(Unconnected);
            }
        }
        if (m_ftp.currentCommand() == QFtp::Put) {
            QString msg = error ?
                (QString("Upload error: ") + m_ftp.errorString()) :
                QString();
            Q_EMIT finished(msg);
            QIODevice *file = m_ftp.currentDevice();
            if (file) {
                file->deleteLater();
            }
        } else if (m_ftp.currentCommand() == QFtp::Mkdir) {
            Q_EMIT dirCreated(!error);
        }
    });

    QObject::connect(&m_ftp, &QFtp::listInfo,
                     [this](const QUrlInfo &urlInfo) {
        if (!urlInfo.isDir()) return;
        m_subdirs.append(urlInfo.name());
        if (!m_subdirsChangedQueued) {
            m_subdirsChangedQueued = true;
            QTimer::singleShot(0, this, [this]() {
                Q_EMIT subdirsChanged(m_subdirs);
            });
        }
    });
}

FtpBackend::~FtpBackend()
{
}

void FtpBackend::login(const QString &host,
                       uint16_t port,
                       const QString &username,
                       const QString &password)
{
    m_ftp.connectToHost(host, port);
    m_ftp.login(username, password);
}

void FtpBackend::mkdir(const QString &dirName)
{
    m_ftp.mkdir(dirName);
}

void FtpBackend::upload(const QString &localFile, const QString &fileName)
{
    QFile *file = new QFile(localFile, this);
    file->open(QIODevice::ReadOnly);
    m_ftp.put(file, fileName);
}

void FtpBackend::logout()
{
    m_ftp.close();
}

void FtpBackend::cd(const QString &dirName)
{
    m_subdirs = QStringList { ".." };
    m_subdirsChangedQueued = false;
    m_ftp.cd(dirName);
    m_ftp.list();
    Q_EMIT subdirsChanged(m_subdirs);
}
