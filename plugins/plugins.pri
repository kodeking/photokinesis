TEMPLATE = lib

include(../common-config.pri)

macx:DESTDIR = $${TOP_BUILD_DIR}/src/PhotoTeleport.app/Contents/plugins

CONFIG += \
    link_pkgconfig \
    plugin

# Error on undefined symbols
QMAKE_LFLAGS += $$QMAKE_LFLAGS_NOUNDEF

QT += \
    network \
    quick

LIBS += \
    -lPhotokinesis
QMAKE_LIBDIR += \
    $${TOP_BUILD_DIR}/lib/Photokinesis

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib

!defined(AUTHENTICATION_LIBS, var):packagesExist(Authentication) {
    PKGCONFIG += Authentication
} else {
    LIBS += $${AUTHENTICATION_LIBS} -lAuthentication
    INCLUDEPATH += $${AUTHENTICATION_INCLUDEPATH}
}

RESOURCES += \
    resources.qrc

target.path = $${INSTALL_PREFIX}/$${PLUGIN_SUBDIR}
INSTALLS += target
