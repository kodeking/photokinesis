import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: root
    Layout.fillWidth: true

    Label {
        text: qsTr("Publish destination:")
    }

    ExclusiveGroup { id: destGroup }

    RadioButton {
        text: qsTr("one of your albums")
        checked: !uploader.groupId
        exclusiveGroup: destGroup
    }

    RadioButton {
        id: groupRadioButton
        text: qsTr("one of your groups")
        checked: uploader.groupId
        exclusiveGroup: destGroup
    }

    ComboBox {
        id: groupSelector
        Layout.fillWidth: true
        enabled: groupRadioButton.checked
        model: uploader.groups
        textRole: "name"
    }

    Binding {
        target: uploader
        property: "groupId"
        value: groupRadioButton.checked ? uploader.groups[groupSelector.currentIndex].id : ""
    }
}
