import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

RowLayout {
    id: root
    Layout.fillWidth: true

    Label {
        Layout.alignment: Qt.AlignTop
        text: qsTr("View permissions:")
    }

    ColumnLayout {
        ExclusiveGroup { id: privacyGroup }

        RadioButton {
            id: privateField
            Layout.fillWidth: true
            text: qsTr("Private (only me)")
            checked: !uploader.isPublic
            exclusiveGroup: privacyGroup
        }

        CheckBox {
            Layout.leftMargin: 20
            text: qsTr("My family")
            enabled: privateField.checked
            checked: uploader.isFamily
            onClicked: uploader.isFamily = checked
        }

        CheckBox {
            Layout.leftMargin: 20
            text: qsTr("My friends")
            enabled: privateField.checked
            checked: uploader.isFriends
            onClicked: uploader.isFriends = checked
        }

        RadioButton {
            Layout.fillWidth: true
            text: qsTr("Public (anyone)")
            checked: uploader.isPublic
            exclusiveGroup: privacyGroup
            onCheckedChanged: uploader.isPublic = checked
        }
    }
}
