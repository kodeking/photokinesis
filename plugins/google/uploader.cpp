/*
 * Copyright (C) 2017-2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Authentication/OAuth2>
#include <Photokinesis/AbstractUploader>
#include <Photokinesis/SignalWaiter>
#include <Photokinesis/UiHelpers>
#include <QByteArray>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QDomText>
#include <QFileInfo>
#include <QHttpMultiPart>
#include <QHttpPart>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <QUrlQuery>

using namespace Photokinesis;

static const QString baseUrl =
    QStringLiteral("https://picasaweb.google.com/data/feed/api/");

namespace Photokinesis {

class GoogleReply: public QObject
{
    Q_OBJECT

public:
    GoogleReply(QNetworkReply *reply, QObject *parent = 0):
        QObject(parent),
        m_reply(reply)
    {
        QObject::connect(reply, &QNetworkReply::finished,
                         this, [this]() {
            QByteArray replyText = m_reply->readAll();
            qDebug() << "raw reply:" << replyText;
            m_json = QJsonDocument::fromJson(replyText).object();
            if (m_json.isEmpty()) {
                QDomDocument doc;
                doc.setContent(replyText);
                m_xml = doc.documentElement();
                if (m_xml.isNull()) {
                    m_errorMessage = replyText;
                }
            }
            deleteLater();
        });
        QObject::connect(reply, &QNetworkReply::finished,
                         this, &GoogleReply::finished);
    }

    QNetworkReply *networkReply() const { return m_reply.data(); }
    QJsonObject response() const { return m_json; }
    QDomElement xmlResponse() const { return m_xml; }

    bool isError() const { return !m_errorMessage.isEmpty(); }
    QString errorMessage() const { return m_errorMessage; }
    bool needsAuthentication() const {
        // TODO
        return false;
    }

Q_SIGNALS:
    void finished();

private:
    QJsonObject m_json;
    QDomElement m_xml;
    QString m_errorMessage;
    QScopedPointer<QNetworkReply> m_reply;
};

class GoogleUploader: public AbstractUploader
{
    Q_OBJECT

public:
    GoogleUploader(const QJsonObject &metadata, QObject *parent = 0);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;

    QJsonObject findAlbum(const QString &id) const;

    GoogleReply *call(const QString &method,
                      const QVariantMap &params = QVariantMap());
    GoogleReply *callPlus(const QString &method,
                          const QVariantMap &params = QVariantMap());
    GoogleReply *getProfileInfo();
    GoogleReply *getAlbums();

    void initialize();
    bool checkAuthentication(GoogleReply *reply);

private Q_SLOTS:
    void onAuthenticationFinished();

private:
    Authentication::OAuth2 m_auth;
    Authentication::OAuth2 m_pageAuth;
    QQuickItem *m_parentItem;
    QJsonObject m_profileInfo;
};

} // namespace

GoogleUploader::GoogleUploader(const QJsonObject &metadata,
                                   QObject *parent):
    AbstractUploader(metadata, parent),
    m_parentItem(0)
{
    m_auth.setClientId("261797508896-0asp3so71qd1th0mopsgc9eknb2atbo8.apps.googleusercontent.com");
    m_auth.setClientSecret("uoRqU7zRiTFWLQ3mcCECLopI");
    m_auth.setScopes({
        "https://picasaweb.google.com/data/",
        "profile",
    });
    m_auth.setAuthorizationUrl(QUrl("https://accounts.google.com/o/oauth2/"
                                    "v2/auth?access_type=offline"));
    m_auth.setAccessTokenUrl(QUrl("https://www.googleapis.com/oauth2/v4/token"));
    m_auth.setResponseType("code");
    m_auth.setCallbackUrl("https://www.mardy.it/oauth/teleport");
    m_auth.setUserAgent("Photokinesis/1.0");
    m_auth.setProtocolTweaks(Authentication::OAuth2::ClientAuthInRequestBody);

    QObject::connect(&m_auth, SIGNAL(finished()),
                     this, SLOT(onAuthenticationFinished()));
}

void GoogleUploader::authenticate(QQuickItem *parent)
{
    setStatus(AbstractUploader::Authenticating);
    UiHelpers::createWebview(parent, &m_auth);
    m_auth.process();
}

void GoogleUploader::logout()
{
    m_auth.logout();
    QUrl url = m_auth.authorizationUrl();
    QUrlQuery query(url);
    if (!query.hasQueryItem("prompt")) {
        query.addQueryItem("prompt", "select_account");
        url.setQuery(query);
        m_auth.setAuthorizationUrl(url);
    }
    AbstractUploader::logout();
}

void GoogleUploader::startUpload(const QString &filePath, const QJsonObject &options)
{
    setStatus(AbstractUploader::Busy);

    QJsonObject album = findAlbum(defaultAlbumId());

    QStringList tags;
    QJsonArray tagArray = options["tags"].toArray();
    for (const QJsonValue &v: tagArray) {
        tags.append(v.toString());
    }

    QFileInfo fileInfo(filePath);

    QDomDocument doc;
    QDomProcessingInstruction instr =
        doc.createProcessingInstruction(QStringLiteral("xml"),
                                        QStringLiteral("version='1.0' encoding='UTF-8'"));
    doc.appendChild(instr);
    QDomElement entryElem = doc.createElement(QStringLiteral("entry"));
    doc.appendChild(entryElem);
    entryElem.setAttribute(QStringLiteral("xmlns"),
                           QStringLiteral("http://www.w3.org/2005/Atom"));
    QDomElement titleElem = doc.createElement(QStringLiteral("title"));
    entryElem.appendChild(titleElem);
    QDomText titleText = doc.createTextNode(fileInfo.fileName());
    titleElem.appendChild(titleText);
    QDomElement summaryElem = doc.createElement(QStringLiteral("summary"));
    entryElem.appendChild(summaryElem);
    QDomText summaryText = doc.createTextNode(options["description"].toString());
    summaryElem.appendChild(summaryText);
    QDomElement categoryElem = doc.createElement(QStringLiteral("category"));
    entryElem.appendChild(categoryElem);
    categoryElem.setAttribute(QStringLiteral("scheme"),
                              QStringLiteral("http://schemas.google.com/g/2005#kind"));
    categoryElem.setAttribute(QStringLiteral("term"),
                              QStringLiteral("http://schemas.google.com/photos/2007#photo"));
    QDomElement mediaGroupElem = doc.createElementNS(QStringLiteral("http://search.yahoo.com/mrss/"),
                                                     QStringLiteral("media:group"));
    entryElem.appendChild(mediaGroupElem);
    QDomElement mediaKeywordsElem =
        doc.createElementNS(QStringLiteral("http://search.yahoo.com/mrss/"),
                            QStringLiteral("media:keywords"));
    mediaGroupElem.appendChild(mediaKeywordsElem);
    QDomText mediaKeywordsText = doc.createTextNode(tags.join(','));
    mediaKeywordsElem.appendChild(mediaKeywordsText);

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::RelatedType);

    QHttpPart metaPart;
    metaPart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/atom+xml"));
    metaPart.setBody(doc.toString().toUtf8());
    multiPart->append(metaPart);

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg")); // FIXME
    QFile *file = new QFile(fileInfo.filePath(), multiPart);
    file->open(QIODevice::ReadOnly);
    imagePart.setBodyDevice(file);
    multiPart->append(imagePart);

    QUrl endpoint(baseUrl + album["uploadUrl"].toString());
    qDebug() << "Uploading to" << endpoint;
    QNetworkRequest req = m_auth.authorizedRequest("POST", endpoint);
    QNetworkAccessManager *nam = m_auth.networkAccessManager();
    GoogleReply *reply = new GoogleReply(nam->post(req, multiPart));
    multiPart->setParent(reply);
    QObject::connect(reply->networkReply(), &QNetworkReply::uploadProgress,
                     this, [this](qint64 bytesSent, qint64 bytesTotal) {
        if (bytesTotal > 0) {
            Q_EMIT progress(bytesSent * 100 / bytesTotal);
        }
    });
    QObject::connect(reply, &GoogleReply::finished, this, [this, reply]() {
        QDomElement response = reply->xmlResponse();
        if (response.tagName() == "entry") {
            setUploadsUrl(QUrl(findAlbum(defaultAlbumId())["link"].toString()));
            Q_EMIT done(true);
        } else {
            setErrorMessage("Photo upload failed: " + reply->errorMessage());
            Q_EMIT done(false);
        }
        setStatus(AbstractUploader::Ready);
    });
}

QJsonObject GoogleUploader::findAlbum(const QString &id) const
{
    QJsonArray albums = this->albums();
    for (const QJsonValue &v: albums) {
        QJsonObject o = v.toObject();
        if (o.value("albumId").toString() == id) {
            return o;
        }
    }
    return QJsonObject();
}

GoogleReply *GoogleUploader::call(const QString &method, const QVariantMap &params)
{
    QUrl url(baseUrl + method + "?v=2&alt=json");
    return new GoogleReply(m_auth.get(url, params));
}

GoogleReply *GoogleUploader::callPlus(const QString &method,
                                      const QVariantMap &params)
{
    QUrl url("https://www.googleapis.com/plus/v1/" + method);
    return new GoogleReply(m_auth.get(url, params));
}

GoogleReply *GoogleUploader::getProfileInfo()
{
    GoogleReply *reply = callPlus(QStringLiteral("people/me"));
    QObject::connect(reply, &GoogleReply::finished, this, [this, reply]() {
        checkAuthentication(reply);
        m_profileInfo = reply->response();
    });
    return reply;
}

GoogleReply *GoogleUploader::getAlbums()
{
    QVariantMap params {
        { "kind", "album" },
        { "fields", "entry(id,media:group/media:title,link)" },
    };
    GoogleReply *reply = call(QStringLiteral("user/default"), params);
    QObject::connect(reply, &GoogleReply::finished, this, [this, reply]() {
        const QJsonArray netAlbums = reply->response().value("feed")
            .toObject().value("entry").toArray();
        QJsonArray albums;
        for (const QJsonValue &v: netAlbums) {
            const QJsonObject s = v.toObject();
            QJsonObject a;
            a.insert("name", s["media$group"].toObject()["media$title"]
                     .toObject()["$t"].toString());
            QString id = s["id"].toObject()["$t"].toString();
            a.insert("albumId", id);
            QString link;
            QJsonArray links = s["link"].toArray();
            for (const QJsonValue &l: links) {
                QJsonObject lo = l.toObject();
                if (lo["type"].toString() == "text/html") {
                    link = lo["href"].toString();
                }
            }
            a.insert("link", link);
            a.insert("uploadUrl", id.mid(id.indexOf("/user/") + 1));
            albums.append(a);
        }

        if (defaultAlbumId().isEmpty() && !albums.isEmpty()) {
            setDefaultAlbumId(albums.first().toObject().value("albumId").toString());
        }
        setAlbums(albums);
    });
    return reply;
}

void GoogleUploader::initialize()
{
    setStatus(AbstractUploader::RetrievingAccountInfo);

    SignalWaiter *waiter = new SignalWaiter(SIGNAL(finished()), this);
    QObject::connect(waiter, &SignalWaiter::finished,
                    this, [this, waiter]() {
        waiter->deleteLater();
        if (status() == AbstractUploader::NeedsAuthentication) return;

        QString name = m_profileInfo.value("displayName").toString();
        setUserId(name);

        loadSettings();

        /* TODO: find something meaningful for accountInfo */
        setAccountInfo(QString());
        Q_EMIT accountInfoChanged();

        setStatus(AbstractUploader::Ready);
    });

    waiter->add(getAlbums());
    waiter->add(getProfileInfo());
}

bool GoogleUploader::checkAuthentication(GoogleReply *reply)
{
    if (reply->isError() && reply->needsAuthentication()) {
        qDebug() << "Needs authentication!";
        m_auth.logout();
        setStatus(AbstractUploader::NeedsAuthentication);
        return false;
    }
    return true;
}

void GoogleUploader::onAuthenticationFinished()
{
    if (!m_auth.hasError()) {
        initialize();
    } else {
        qWarning() << "Authentication error:" << m_auth.errorMessage();
        setErrorMessage(m_auth.errorMessage());
        setStatus(AbstractUploader::NeedsAuthentication);
    }
}

Photokinesis::AbstractUploader *
GooglePlugin::createUploader(const QJsonObject &metadata,
                               QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new GoogleUploader(metadata, parent);
}

#include "uploader.moc"
