import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

RowLayout {
    id: root
    Layout.fillWidth: true

    property var albums: uploader.albums

    Label {
        text: qsTr("Destination album:")
    }
    ComboBox {
        id: comboBox
        Layout.fillWidth: true
        model: root.albums
        textRole: "name"
        onActivated: {
            var id = root.albums[index].albumId
            uploader.defaultAlbumId = id
        }

        onModelChanged: selectDefault()

        Connections {
            target: uploader
            onDefaultAlbumIdChanged: comboBox.selectDefault()
        }

        function selectDefault() {
            var defaultAlbumId = uploader.defaultAlbumId
            if (!defaultAlbumId) return

            for (var i = 0; i < model.length; i++) {
                if (model[i].albumId == defaultAlbumId) {
                    currentIndex = i
                    break
                }
            }
        }
    }
}
