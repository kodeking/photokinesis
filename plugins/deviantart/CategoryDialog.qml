import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: root

    property var utils: null

    title: qsTr("Select category")

    property var _initialPath: []

    Component.onCompleted: _initialPath = uploader.category

    contentItem: ColumnLayout {
        id: internal
        anchors.fill: parent

        Label {
            text: qsTr("Current category:")
        }

        Rectangle {
            Layout.fillWidth: true
            color: "transparent"
            border { color: palette.shadow; width: 1 }
            implicitHeight: dirLabel.implicitHeight + 4
            implicitWidth: dirLabel.implicitWidth
            Label {
                id: dirLabel
                anchors { fill: parent; margins: 2 }
                text: utils.beautifyCategory(uploader.category)
                horizontalAlignment: Text.AlignHCenter
                elide: Text.ElideLeft
                font { pixelSize: 16; italic: true }
            }
        }

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle {
                anchors.fill: parent
                color: palette.base
                border { color: palette.shadow; width: 1 }
            }

            ListView {
                anchors { fill: parent; margins: 2 }
                model: uploader.subcategories
                delegate: Label {
                    property string icon: modelData.has_subcategory ?
                        "\uD83D\uDCC1" : "\u2022"
                    width: parent.width
                    text: qsTr("%1 %2").arg(icon).arg(modelData.title)
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            var newCategory = uploader.category
                            newCategory.push(modelData)
                            uploader.category = newCategory
                        }
                    }
                }
                header: uploader.category.length > 0 ? headerComponent : null
            }
        }

        RowLayout {
            anchors { right: parent.right }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: {
                    uploader.category = _initialPath
                    root.close()
                }
            }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("OK")
                iconName: "dialog-ok"
                enabled: utils.isLeaf(uploader.category)
                onClicked: {
                    root.close()
                }
            }
        }
    }

    SystemPalette { id: palette; colorGroup: SystemPalette.Active } 

    Component {
        id: headerComponent
        Label {
            width: parent ? parent.width : 0
            text: qsTr("\u2190 <i>Go back</i>")
            textFormat: Text.StyledText
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var newCategory = uploader.category
                    newCategory.pop()
                    uploader.category = newCategory
                }
            }
        }
    }
}
