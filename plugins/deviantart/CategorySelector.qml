import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

GridLayout {
    id: root
    Layout.fillWidth: true
    columns: 2

    Label {
        Layout.minimumWidth: implicitWidth
        text: qsTr("Image category:")
    }

    Label {
        Layout.fillWidth: true
        text: root.beautifyCategory(uploader.category)
        elide: Text.ElideLeft
    }

    Button {
        Layout.columnSpan: 2
        Layout.alignment: Qt.AlignRight
        text: qsTr("Change...")
        onClicked: PopupUtils.open(Qt.resolvedUrl("CategoryDialog.qml"), root, {
            "utils": root,
        })
    }

    function beautifyCategory(category) {
        var path = []
        for (var i = 0; i < category.length; i++) {
            path.push(category[i].title)
        }
        return path.join(' / ')
    }

    function isLeaf(category) {
        if (category.length == 0) return false
        return !category[category.length - 1].has_subcategory
    }
}
