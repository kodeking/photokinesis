import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

MyDialog {
    id: root

    property var uploader: null

    title: qsTr("Set a maximum size")
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    onActionChosen: if (action.button == StandardButton.Ok) {
        action.accepted = validateSize()
    }

    GridLayout {
        id: internal
        anchors.centerIn: parent
        columns: 2

        Label {
            Layout.fillWidth: true
            text: qsTr("Maximum width:")
        }

        SpinBox {
            id: widthField
            maximumValue: 99999
            value: uploader.maxSize.width
            onFocusChanged: if (!focus && value > 0 && heightField.value == 0) {
                heightField.value = value
            }
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Maximum height:")
        }

        SpinBox {
            id: heightField
            maximumValue: 99999
            value: uploader.maxSize.height
        }

        Label {
            Layout.columnSpan: 2
            font.italic: true
            wrapMode: Text.Wrap
            text: qsTr("Leave the fields to 0 if no resizing is desired.")
        }

        Label {
            Layout.columnSpan: 2
            font.italic: true
            wrapMode: Text.Wrap
            text: qsTr("In any case, the aspect ratio of the image will be preserved.")
        }
    }

    function validateSize() {
        console.log("Validate size called")
        var size = Qt.size(widthField.value, heightField.value)
        var checkedSize = uploader.checkSize(size)
        uploader.maxSize = checkedSize.size
        return true
    }
}
