// From Ubuntu UI toolkit
function open(popup, caller, params) {
    var popupComponent = null;
    var rootObject = null;
    if (popup.createObject) {
        // popup is a component and can create an object
        popupComponent = popup;
    } else if (typeof popup === "string") {
        popupComponent = Qt.createComponent(popup);
    } else {
        print("PopupUtils.open(): "+popup+" is not a component or a link");
        return null;
    }

    var popupObject;
    if (params !== undefined) {
        popupObject = popupComponent.createObject(caller, params);
    } else {
        popupObject = popupComponent.createObject(caller);
    }
    if (!popupObject) {
        print(popupComponent.errorString().slice(0, -1));
        print("PopupUtils.open(): Failed to create the popup object.");
        return;
    } else if (popupObject.hasOwnProperty("caller") && caller)
        popupObject.caller = caller;

    popupObject.visible = true;
    function onVisibleChanged() {
        if (!popupObject.visible) {
            popupObject.destroy();
        }
    }
    if (popupObject.onVisibleChanged) {
        popupObject.onVisibleChanged.connect(onVisibleChanged);
    } else if (popupObject.visibilityChanged) {
        popupObject.visibilityChanged.connect(onVisibleChanged);
    }
    return popupObject;
}
