import QtQuick 2.0

Item {
    id: root

    property var authenticator: null
    property bool wantsVisibility: webviewLoader.item ? true : false

    signal finished()

    anchors.fill: parent

    Loader {
        id: webviewLoader
        anchors.fill: parent
    }

    Connections {
        target: authenticator
        onFinished: {
            webviewLoader.active = false
            root.finished()
        }
        onActionRequested: {
            webviewLoader.setSource(Qt.resolvedUrl("Webview.qml"), {
                "request": request,
            })
        }
    }

    function cancel() {
        if (webviewLoader.item && webviewLoader.item.request) {
            webviewLoader.item.request.reject()
        }
    }
}
