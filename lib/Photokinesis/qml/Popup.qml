import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Window 2.2

MouseArea {
    id: root

    default property alias content: contentItem.data
    signal closing(var close)

    x: -_origin.x
    y: -_origin.y
    width: Window.width
    height: Window.height

    /* Comma operator is used to force re-evaluation of binding */
    property var _origin: parent.x, parent.y, parent.mapToItem(Window.contentItem, 0, 0)
    property var _parentRect: root.x, root.y, parent.mapToItem(root, 0, 0, parent.width, parent.height)

    onClicked: {
        var event = {
            "accepted": true
        }
        closing(event)
        if (event.accepted) close()
    }

    Item {
        id: contentItem
        x: _parentRect.x
        y: _parentRect.y
        width: _parentRect.width
        height: _parentRect.height
    }

    function close() {
        visible = false
    }
}
