/*
 * Copyright (C) 2017-2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plugin_manager.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QJsonObject>
#include <QLibrary>
#include <QPluginLoader>
#include <QStandardPaths>
#include <QVector>
#include "abstract_uploader.h"
#include "plugin_interface.h"

using namespace Photokinesis;

namespace Photokinesis {

struct Plugin {
    QObject *instance;
    QJsonObject metaData;
};

class PluginManagerPrivate
{
    Q_DECLARE_PUBLIC(PluginManager)

public:
    PluginManagerPrivate(PluginManager *q);
    ~PluginManagerPrivate();

    void loadAll();
    void checkInterfaces();

private:
    static PluginManager *m_instance;
    QVector<Plugin> m_plugins;
    QList<AbstractUploader*> m_uploaders;
    PluginManager *q_ptr;
};

PluginManager *PluginManagerPrivate::m_instance = 0;

} // namespace

PluginManagerPrivate::PluginManagerPrivate(PluginManager *q):
    q_ptr(q)
{
    QCoreApplication::addLibraryPath(PLUGIN_DIR);
    loadAll();
    checkInterfaces();
}

PluginManagerPrivate::~PluginManagerPrivate()
{
    m_instance = 0;
}

void PluginManagerPrivate::loadAll()
{
    /* First, load all static plugins */

    QVector<QStaticPlugin> staticPlugins = QPluginLoader::staticPlugins();
    for (const QStaticPlugin &plugin: staticPlugins) {
        m_plugins.append(Plugin{ plugin.instance(), plugin.metaData() });
    }

    /* Then, find dynamic plugins */

    QString prefix = PLUGIN_DIR[0] == '/' ?
        "" : (QCoreApplication::applicationDirPath() + "/");
    QDir pluginDir(prefix + PLUGIN_DIR);
    QStringList entries = pluginDir.entryList(QDir::NoDotAndDotDot | QDir::Files);
    for (const QString &fileName: entries) {
        if (!QLibrary::isLibrary(fileName)) continue;
        QPluginLoader loader(pluginDir.filePath(fileName));
        QObject *instance = loader.instance();
        if (Q_UNLIKELY(!instance)) {
            qWarning() << "Could not load plugin" << fileName << loader.errorString();
            continue;
        }
        m_plugins.append(Plugin{ instance, loader.metaData() });
    }
}

void PluginManagerPrivate::checkInterfaces()
{
    Q_Q(PluginManager);

    for (const Plugin &plugin: m_plugins) {
        PluginInterface *interface =
            qobject_cast<PluginInterface *>(plugin.instance);
        if (Q_UNLIKELY(!interface)) continue;

        AbstractUploader *uploader =
            interface->createUploader(plugin.metaData, q);
        if (uploader) {
            m_uploaders.append(uploader);
        }
    }
}

PluginManager::PluginManager(QObject *parent):
    QObject(parent),
    d_ptr(new PluginManagerPrivate(this))
{
}

PluginManager::~PluginManager()
{
}

PluginManager *PluginManager::instance()
{
    if (!PluginManagerPrivate::m_instance) {
        PluginManagerPrivate::m_instance = new PluginManager;
    }
    return PluginManagerPrivate::m_instance;
}

QList<AbstractUploader*> PluginManager::uploaders() const
{
    Q_D(const PluginManager);
    return d->m_uploaders;
}
