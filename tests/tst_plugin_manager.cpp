/*
 * Copyright (C) 2017-2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plugin_manager.h"

#include <QDebug>
#include <QScopedPointer>
#include <QTest>
#include <QtPlugin>

using namespace Photokinesis;

class PluginManagerTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testInstance();
    void testPlugins();
};

void PluginManagerTest::testInstance()
{
    QScopedPointer<PluginManager> manager(PluginManager::instance());

    QVERIFY(!manager.isNull());
}

void PluginManagerTest::testPlugins()
{
    QScopedPointer<PluginManager> manager(PluginManager::instance());

    QSet<AbstractUploader *> expectedUploaders {
        reinterpret_cast<AbstractUploader*>(0x10000),
        reinterpret_cast<AbstractUploader*>(0x8000),
    };
    QCOMPARE(manager->uploaders().toSet(), expectedUploaders);
}

Q_IMPORT_PLUGIN(StaticPlugin)

QTEST_GUILESS_MAIN(PluginManagerTest)

#include "tst_plugin_manager.moc"
