TARGET = tst_rounded_image_provider

include(tests.pri)

QT += \
    gui \
    quick

SOURCES += \
    $${SRC_DIR}/rounded_image_provider.cpp \
    tst_rounded_image_provider.cpp

HEADERS += \
    $${SRC_DIR}/rounded_image_provider.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
