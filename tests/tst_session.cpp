/*
 * Copyright (C) 2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "session.h"

#include <QByteArray>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QScopedPointer>
#include <QTemporaryDir>
#include <QTest>
#include "test_utils.h"

using namespace Photokinesis;

class SessionTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testLocation();
    void testIsFinished_data();
    void testIsFinished();
    void testLastSession_data();
    void testLastSession();
};

void SessionTest::testLocation()
{
    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());

    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());

    QByteArray json = R"({
        "key": "value",
        "array": [ "one", "two", "three" ]
    })";
    QJsonDocument doc = QJsonDocument::fromJson(json);
    QJsonObject expectedObject = doc.object();

    {
        Session session;
        session.storeSession(expectedObject);
    }

    {
        Session session;
        QCOMPARE(session.lastSession(), expectedObject);
    }

    QFileInfo fileInfo(
        QStandardPaths::writableLocation(QStandardPaths::CacheLocation) +
        "/lastsession.json");
    QVERIFY(fileInfo.exists());
}

void SessionTest::testIsFinished_data()
{
    QTest::addColumn<QString>("json");
    QTest::addColumn<bool>("expectedIsFinished");

    QTest::newRow("empty") <<
        R"({
          "uploaders": [
            { "name": "anypost", "selected": false },
            { "name": "facebook", "selected": true }
          ],
          "uploadModel": {
            "rows": [],
            "uploadersCount": 2
          }
        })" << true;

    QTest::newRow("one row") <<
        R"({
          "uploaders": [
            { "name": "anypost", "selected": false },
            { "name": "facebook", "selected": true }
          ],
          "uploadModel": {
            "rows": [
              [
                { "description": "Photo" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Ready" }
              ]
            ],
            "uploadersCount": 2
          }
        })" << false;

    QTest::newRow("two rows, done") <<
        R"({
          "uploaders": [
            { "name": "anypost", "selected": false },
            { "name": "facebook", "selected": true }
          ],
          "uploadModel": {
            "rows": [
              [
                { "description": "Photo" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Uploaded" }
              ],
              [
                { "description": "Photo2" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Uploaded" }
              ]
            ],
            "uploadersCount": 2
          }
        })" << true;
}

void SessionTest::testIsFinished()
{
    QFETCH(QString, json);
    QFETCH(bool, expectedIsFinished);

    QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8());

    Session session;
    QCOMPARE(session.sessionIsFinished(doc.object()), expectedIsFinished);
}

void SessionTest::testLastSession_data()
{
    QTest::addColumn<QString>("json");
    QTest::addColumn<QStringList>("currentUploaders");
    QTest::addColumn<QString>("expectedJson");

    QTest::newRow("empty, one less uploader") <<
        R"({
          "uploaders": [
            { "name": "facebook", "selected": true },
            { "name": "flickr", "selected": true }
          ],
          "uploadModel": {
            "rows": [],
            "uploadersCount": 2
          }
        })" <<
        QStringList { "flickr" } <<
        R"({
          "uploaders": [
            { "name": "flickr", "selected": true }
          ],
          "uploadModel": {
            "rows": [],
            "uploadersCount": 1
          }
        })";

    QTest::newRow("two rows, uploaders swapped, new uploaders") <<
        R"({
          "uploaders": [
            { "name": "facebook", "selected": true },
            { "name": "flickr", "selected": true }
          ],
          "uploadModel": {
            "rows": [
              [
                { "description": "Photo" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Uploaded" }
              ],
              [
                { "description": "Photo2" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Uploaded" }
              ]
            ],
            "uploadersCount": 2
          }
        })" <<
        QStringList { "other", "flickr", "other2", "facebook" } <<
        R"({
          "uploaders": [
            { "name": "other" },
            { "name": "flickr", "selected": true },
            { "name": "other2" },
            { "name": "facebook", "selected": true }
          ],
          "uploadModel": {
            "rows": [
              [
                { "description": "Photo" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Uploaded" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Ready" }
              ],
              [
                { "description": "Photo2" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Uploaded" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Ready" }
              ]
            ],
            "uploadersCount": 4
          }
        })";

    QTest::newRow("fewer uploaders") <<
        R"({
          "uploaders": [
            { "name": "facebook", "selected": true },
            { "name": "removed", "selected": true },
            { "name": "flickr", "selected": true }
          ],
          "uploadModel": {
            "rows": [
              [
                { "description": "Photo" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Failed" },
                { "uploadStatus": "Uploaded" }
              ],
              [
                { "description": "Photo2" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Uploaded" },
                { "uploadStatus": "Failed" }
              ]
            ],
            "uploadersCount": 3
          }
        })" <<
        QStringList { "other", "flickr", "facebook" } <<
        R"({
          "uploaders": {
            "flickr": { "index": 2 },
            "facebook": { "index": 3 }
          },
          "uploaders": [
            { "name": "other" },
            { "name": "flickr", "selected": true },
            { "name": "facebook", "selected": true }
          ],
          "uploadModel": {
            "rows": [
              [
                { "description": "Photo" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Uploaded" },
                { "uploadStatus": "Ready" }
              ],
              [
                { "description": "Photo2" },
                { "uploadStatus": "Ready" },
                { "uploadStatus": "Failed" },
                { "uploadStatus": "Ready" }
              ]
            ],
            "uploadersCount": 3
          }
        })";
}

void SessionTest::testLastSession()
{
    QFETCH(QString, json);
    QFETCH(QStringList, currentUploaders);
    QFETCH(QString, expectedJson);

    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());

    QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8());
    QJsonObject expectedObject =
        QJsonDocument::fromJson(expectedJson.toUtf8()).object();

    Session session;
    session.storeSession(doc.object());
    QJsonObject newSession = session.lastSession(currentUploaders);
    QCOMPARE(newSession, expectedObject);
}

QTEST_GUILESS_MAIN(SessionTest)

#include "tst_session.moc"
