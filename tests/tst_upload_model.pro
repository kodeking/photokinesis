TARGET = tst_upload_model

include(tests.pri)

LIBS += \
    -lPhotokinesis
QMAKE_LIBDIR += \
    $${TOP_BUILD_DIR}/lib/Photokinesis
QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    tst_upload_model.cpp

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
