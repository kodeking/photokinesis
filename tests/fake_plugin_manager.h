/*
 * Copyright (C) 2017-2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_PLUGIN_MANAGER_H
#define FAKE_PLUGIN_MANAGER_H

#include "plugin_manager.h"

#include <QObject>

namespace Photokinesis {

class PluginManagerPrivate
{
public:
    static PluginManagerPrivate *mocked(PluginManager *o) { return o->d_ptr.data(); }

    void setUploaders(const QList<AbstractUploader*> &uploaders) {
        m_uploaders = uploaders;
    }

private:
    friend class PluginManager;
    QList<AbstractUploader*> m_uploaders;
};

} // namespace

#endif // FAKE_PLUGIN_MANAGER_H
