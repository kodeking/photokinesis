TARGET = tst_ui_helpers

include(tests.pri)

QT += \
    quick \
    webview

LIBS += \
    -lPhotokinesis
QMAKE_LIBDIR += \
    $${TOP_BUILD_DIR}/lib/Photokinesis
QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    tst_ui_helpers.cpp

check.commands = "xvfb-run -s '-screen 0 640x480x24' -a ./$${TARGET}"
