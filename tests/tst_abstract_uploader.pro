TARGET = tst_abstract_uploader

include(tests.pri)

QT += \
    network \
    qml \
    quick

HEADERS += \
    $${PHOTOKINESIS_SRC_DIR}/abstract_uploader.h \
    $${PHOTOKINESIS_SRC_DIR}/json_storage.h \
    fake_image_processor.h

SOURCES += \
    $${PHOTOKINESIS_SRC_DIR}/abstract_uploader.cpp \
    $${PHOTOKINESIS_SRC_DIR}/json_storage.cpp \
    tst_abstract_uploader.cpp
