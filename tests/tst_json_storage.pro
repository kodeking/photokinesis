TARGET = tst_json_storage

include(tests.pri)

QT += qml

LIBS += \
    -lPhotokinesis
QMAKE_LIBDIR += \
    $${TOP_BUILD_DIR}/lib/Photokinesis
QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    tst_json_storage.cpp
