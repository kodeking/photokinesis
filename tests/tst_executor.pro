TARGET = tst_executor

include(tests.pri)

LIBS += \
    -lPhotokinesis
QMAKE_LIBDIR += \
    $${TOP_BUILD_DIR}/lib/Photokinesis
QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    $${SRC_DIR}/executor.cpp \
    tst_executor.cpp

HEADERS += \
    $${SRC_DIR}/executor.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
