TARGET = tst_session

include(tests.pri)

QT += qml

LIBS += \
    -lPhotokinesis
QMAKE_LIBDIR += \
    $${TOP_BUILD_DIR}/lib/Photokinesis
QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    $${SRC_DIR}/session.cpp \
    tst_session.cpp

HEADERS += \
    $${SRC_DIR}/session.h
