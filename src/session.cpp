/*
 * Copyright (C) 2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "session.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QSet>

using namespace Photokinesis;

namespace Photokinesis {

class SessionPrivate
{
    friend class Session;

public:
    SessionPrivate();

    static QJsonObject findUploader(const QJsonArray &uploaders,
                                    const QString &name,
                                    int *foundIndex);
    QJsonObject fixSession(const QJsonObject &session,
                           const QStringList &currentUploaders) const;

private:
    QStringList m_currentUploaders;
};

} // namespace

SessionPrivate::SessionPrivate()
{
}

QJsonObject SessionPrivate::findUploader(const QJsonArray &uploaders,
                                         const QString &name,
                                         int *foundIndex)
{
    int i = 0;
    for (const QJsonValue &v: uploaders) {
        const QJsonObject o = v.toObject();
        if (o["name"].toString() == name) {
            *foundIndex = i;
            return o;
        }
        i++;
    }
    return QJsonObject({{ "name", name }});
}

QJsonObject SessionPrivate::fixSession(const QJsonObject &session,
                                       const QStringList &currentUploaders) const
{
    int newUploaderCount = currentUploaders.count();
    /* Maps the new array indexes to the indexes of the old source array */
    QVector<int> columnMap(newUploaderCount, -1);

    const QJsonArray uploaders = session["uploaders"].toArray();
    QJsonArray newUploaders;
    for (int i = 0; i < newUploaderCount; i++) {
        const QString &key = currentUploaders[i];
        int oldIndex = -1;
        QJsonObject newObject(findUploader(uploaders, key, &oldIndex));
        columnMap[i] = oldIndex;
        newUploaders.append(newObject);
    }

    QJsonObject fixed(session);
    fixed["uploaders"] = newUploaders;
    QJsonValueRef modelRef = fixed["uploadModel"];
    QJsonObject model = modelRef.toObject();
    model["uploadersCount"] = newUploaderCount;
    QJsonValueRef rowsRef = model["rows"];
    QJsonArray rows = rowsRef.toArray();
    QJsonObject defaultUploaderData {
        { "uploadStatus", "Ready" },
    };
    for (int i = 0; i < rows.count(); i++) {
        QJsonValueRef rowRef = rows[i];
        QJsonArray row = rowRef.toArray();
        QJsonArray newRow { row.first() };
        for (int col = 0; col < newUploaderCount; col++) {
            int oldIndex = columnMap[col];
            if (oldIndex >= 0) {
                newRow.append(row[oldIndex + 1]);
            } else {
                newRow.append(defaultUploaderData);
            }
        }
        rowRef = newRow;
    }
    rowsRef = rows;
    modelRef = model;
    return fixed;
}

Session::Session(QObject *parent):
    JsonStorage(parent),
    d_ptr(new SessionPrivate())
{
    setBase(QStandardPaths::CacheLocation);
    setFilePath("lastsession.json");
}

Session::~Session()
{
}

void Session::storeSession(const QJsonObject &session)
{
    storeObject(session);
}

QJsonObject Session::lastSession() const
{
    return loadObject();
}

QJsonObject Session::lastSession(const QStringList &uploaders) const
{
    Q_D(const Session);
    QJsonObject session = loadObject();
    return d->fixSession(session, uploaders);
}

bool Session::sessionIsFinished(const QJsonObject &session) const
{
    QSet<int> uploaderColumns;

    QJsonArray uploaders = session["uploaders"].toArray();
    int i = 1;
    for (const QJsonValue &v: uploaders) {
        const QJsonObject o = v.toObject();
        if (o["selected"].toBool() == true) {
            uploaderColumns.insert(i);
        }
        i++;
    }

    QJsonObject model = session["uploadModel"].toObject();
    QJsonArray rows = model["rows"].toArray();
    for (const QJsonValue &v: rows) {
        QJsonArray row = v.toArray();
        for (int column: uploaderColumns) {
            QJsonObject cellData = row.at(column).toObject();
            if (Q_UNLIKELY(cellData.isEmpty())) {
                qWarning() << "Missing column data!";
                return true;
            }

            QString status = cellData["uploadStatus"].toString();
            if (status != "Uploaded") {
                return false;
            }
        }
    }
    return true;
}
