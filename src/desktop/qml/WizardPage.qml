import QtQuick 2.5
import QtQuick.Controls 1.4

Tab {
    id: root

    property string nextButtonLabel: ""
    readonly property bool done: item ? (item.hasOwnProperty("done") ? item.done : true) : false
}
