import QtQuick 2.5

Canvas {
    property color color: "black"
    property int arcWidth: 2
    property real progress: 0 
    property int radius: (width - arcWidth) / 2

    onProgressChanged: requestPaint()

    onPaint: {
        var ctx = getContext("2d")
        var text,text_w
        ctx.clearRect(0, 0, width, height)

        var r = progress * Math.PI / 50
        ctx.beginPath()
        ctx.strokeStyle = color
        ctx.lineWidth = arcWidth

        ctx.arc(width / 2, height / 2, radius, 0 - Math.PI/2, r - Math.PI/2, false)
        ctx.stroke()
    }
}
