import QtQuick 2.2

Rectangle {
    id: root

    property alias icon: image.source
    property bool hasValue: false
    property bool selected: false
    property alias containsMouse: mouseArea.containsMouse

    signal clicked()

    radius: 4
    color: "white"
    border {
        width: 4
        color: selected ? "lightsteelblue" : "white"
    }

    Image {
        id: image
        anchors { fill: parent; margins: parent.border.width }
        sourceSize { width: width; height: height }
        fillMode: Image.PreserveAspectCrop
        smooth: true
        opacity: root.hasValue ? 1 : 0.5
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: root.clicked()
    }
}
