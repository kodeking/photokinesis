import Photokinesis 1.0
import QtQuick 2.5

Image {
    id: root

    property int uploadStatus: UploadModel.Ready
    property alias progress: progressCircle.progress

    height: width
    sourceSize { width: width; height: height }
    smooth: true

    ProgressCircle {
        id: progressCircle
        anchors.fill: parent
        color: "#4de44d"
        arcWidth: width / 4
        visible: root.uploadStatus == UploadModel.Uploading

        Behavior on progress {
            NumberAnimation { duration: 300 }
        }
    }

    Image {
        anchors { right: parent.right; bottom: parent.bottom }
        width: root.width / 2
        height: width
        sourceSize { width: width; height: height }
        smooth: true
        visible: root.uploadStatus > UploadModel.Uploading

        source: root.uploadStatus == UploadModel.Failed ?
            "qrc:/icons/upload-failed" : "qrc:/icons/upload-success"
    }
}
