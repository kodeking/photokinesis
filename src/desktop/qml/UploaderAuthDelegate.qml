import Photokinesis 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Rectangle {
    id: root

    property string displayName: ""
    property alias icon: image.source
    property var uploader: null

    signal accountChangeRequested()
    signal authenticationRequested()

    height: Math.max(columnLayout.height, uploaderOptions.height) + columnLayout.anchors.margins * 2
    color: palette.window
    border { color: palette.shadow; width: 2 }

    onUploaderChanged: uploader.optionsContainer = uploaderOptions

    ColumnLayout {
        id: columnLayout
        anchors { left: parent.left; top: parent.top; margins: 12 }
        width: parent.width * 2 / 5

        Item {
            Layout.fillWidth: true
            height: 80
            Image {
                id: image
                anchors {
                    left: parent.left; top: parent.top; bottom: parent.bottom
                    margins: 8
                }
                width: height
                sourceSize { width: width; height: height }
                fillMode: Image.PreserveAspectFit
                smooth: true
            }

            Text {
                anchors {
                    left: image.right; right: parent.right
                    top: parent.top; bottom: parent.bottom
                    margins: 8
                }
                text: root.displayName
                verticalAlignment: Text.AlignVCenter
            }
        }

        Text {
            id: statusLabel
            Layout.fillWidth: true
        }

        Text {
            id: accountInfo
            visible: false
            text: uploader.accountInfo
            onLinkActivated: Qt.openUrlExternally(link)
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: 12

            Button {
                id: authButton
                Layout.minimumWidth: parent.width / 2
                visible: false
                text: qsTr("Change account...")
                onClicked: root.accountChangeRequested()
            }

            BusyIndicator {
                running: uploader.status == AbstractUploader.Busy
            }
        }
    }

    ColumnLayout {
        id: uploaderOptions
        anchors { top: parent.top; left: columnLayout.right; right: parent.right; margins: 12 }
        enabled: false
    }

    SystemPalette { id: palette }

    states: [
        State {
            name: "base"
        },
        State {
            name: "authenticationFailed"
            when: uploader.status == AbstractUploader.NeedsAuthentication && uploader.errorMessage
            PropertyChanges { target: statusLabel; text: qsTr("Authentication failed: %1").arg(uploader.errorMessage) }
            PropertyChanges {
                target: authButton
                visible: true
                text: qsTr("Try logging in again")
                onClicked: root.authenticationRequested()
            }
        },
        State {
            name: "needsAuthentication"
            when: uploader.status == AbstractUploader.NeedsAuthentication
            PropertyChanges { target: statusLabel; text: qsTr("Waiting for authentication") }
        },
        State {
            name: "authenticating"
            when: uploader.status == AbstractUploader.Authenticating
            PropertyChanges { target: statusLabel; text: qsTr("Logging in...") }
        },
        State {
            name: "accountInfo"
            when: uploader.status == AbstractUploader.RetrievingAccountInfo
            PropertyChanges { target: statusLabel; text: qsTr("Retrieving account info...") }
        },
        State {
            name: "ready"
            when: root.uploader.status == AbstractUploader.Ready
            PropertyChanges { target: statusLabel; text: qsTr("Logged in as %1").arg(uploader.userName) }
            PropertyChanges { target: uploaderOptions; enabled: true }
            PropertyChanges { target: accountInfo; visible: true }
            PropertyChanges { target: authButton; visible: true }
        }
    ]
}
