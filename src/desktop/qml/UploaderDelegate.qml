import QtQuick 2.2

Item {
    id: root

    property int offsetX: 0
    property string displayName: ""
    property alias icon: image.source
    property bool enabled: true
    property bool selected: false
    property var uploader: null
    signal clicked()

    Rectangle {
        property int margin: 2
        x: offsetX + margin
        y: margin
        width: parent.width - margin * 2
        height: parent.height - margin * 2
        color: "white"
        border {
            width: 4
            color: selected ? "lightsteelblue" : "white"
        }
        radius: 4

        Image {
            id: image
            anchors {
                left: parent.left; top: parent.top; bottom: parent.bottom
                margins: 8
            }
            width: height
            sourceSize { width: width; height: height }
            fillMode: Image.PreserveAspectFit
            smooth: true
        }

        Text {
            anchors {
                left: image.right; right: parent.right
                top: parent.top; bottom: parent.bottom
                margins: 8
            }
            text: root.displayName
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: root.clicked()
        }
    }
}
