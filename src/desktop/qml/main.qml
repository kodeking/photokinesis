import Photokinesis 1.0
import SortFilterProxyModel 0.2
import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ApplicationWindow {
    id: root
    visible: true

    contentItem {
        minimumWidth: 300; minimumHeight: 200
        implicitWidth: 800; implicitHeight: 600
    }

    menuBar: MenuBar {
        Menu {
            title: "Help"

            MenuItem {
                text: "About"
                onTriggered: PopupUtils.open(Qt.resolvedUrl("About.qml"), root, {})
            }
        }
    }

    Wizard {
        anchors { fill: parent; margins: 8 }
        allowCancel: false
        onClose: root.close()

        WizardPage {
            ItemsPage {
                model: uploadModel
                sessionHandler: sessionHandlerId
            }
        }

        WizardPage {
            UploadersPage {
                property bool _completed: false
                model: uploaderModel
                Component.onCompleted: {
                    selectedIndexes = selectedUploaders.selectedIndexes
                    _completed = true
                }
                onSelectedIndexesChanged: if (_completed) {
                    selectedUploaders.selectedIndexes = selectedIndexes
                }
            }
        }

        WizardPage {
            AuthenticationPage {
                model: selectedUploaders
            }
        }

        WizardPage {
            ItemOptionsPage {
                items: uploadModel
                uploaders: selectedUploaders
            }
        }

        WizardPage {
            ItemOptionSummaryPage {
                items: uploadModel
                uploaders: selectedUploaders
            }
        }

        WizardPage {
            UploadPage {
                items: uploadModel
                uploaders: selectedUploaders
            }
        }

        WizardPage {
            ResultsPage {
                items: uploadModel
                uploaders: selectedUploaders
            }
        }
    }

    UploadModel {
        id: uploadModel
        uploadersCount: uploaderModel.count
    }

    UploaderModel {
        id: uploaderModel
    }

    SortFilterProxyModel {
        id: selectedUploaders
        property var selectedIndexes: []
        property var supportedOptions: []
        sourceModel: uploaderModel
        filters: [
            ExpressionFilter {
                expression: selectedUploaders.selectedIndexes.indexOf(model.index) >= 0
            }
        ]

        onCountChanged: buildOptions()

        function buildOptions() {
            var options = []
            for (var i = 0; i < count; i++) {
                var uploader = get(i, "uploader")
                var uploaderOptions = uploader.supportedOptions
                for (var j = 0; j < uploaderOptions.length; j++) {
                    var name = uploaderOptions[j]
                    if (options.indexOf(name) < 0) {
                        options.push(name)
                    }
                }
            }
            supportedOptions = options
        }

        function hasOption(name) {
            return supportedOptions.indexOf(name) >= 0
        }

        function column(uploaderIndex) {
            return selectedIndexes[uploaderIndex] + 1
        }

        function uploaderIndex(column) {
            return selectedIndexes.indexOf(column - 1)
        }
    }

    SessionHandler {
        id: sessionHandlerId
        uploadModel: uploadModel
        uploaders: selectedUploaders
    }
}
