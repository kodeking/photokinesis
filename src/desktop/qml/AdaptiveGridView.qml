import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Window 2.2

GridView {
    id: root

    property int itemSize: 128 * Screen.devicePixelRatio
    property int itemMaxSize: 256 * Screen.devicePixelRatio
    property int _itemsPerRow: width / itemSize

    cellWidth: Math.floor(width / _itemsPerRow)
    cellHeight: cellWidth
}
