import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Item {
    id: root

    property var tags: []
    property bool editable: false

    signal tagClicked(int index)

    implicitHeight: flow.implicitHeight + 8
    implicitWidth: flow.implicitWidth + 8

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.1
        radius: 4
    }

    Flow {
        id: flow
        anchors { fill: parent; margins: 4 }
        spacing: 4

        Repeater {
            model: root.tags
            Rectangle {
                width: contents.width + radius
                height: contents.height + radius
                color: (dataIsString || modelData.freq == 1) ? "white" : "#f8f8f8"
                radius: 8
                property bool dataIsString: typeof(modelData) == "string"

                RowLayout {
                    id: contents
                    anchors.centerIn: parent
                    spacing: 1
                    Label {
                        id: tagLabel
                        width: Math.min(flow.width - 16, implicitWidth)
                        verticalAlignment: Text.AlignVCenter
                        text: dataIsString ? modelData : modelData.name
                        elide: Text.ElideRight
                        opacity: (dataIsString || modelData.freq == 1) ? 1 : 0.5
                    }
                    Image {
                        Layout.preferredWidth: width
                        Layout.preferredHeight: height
                        visible: root.editable
                        width: 12
                        height: width
                        mipmap: true
                        source: "qrc:/icons/remove-tag"
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    enabled: root.editable
                    onClicked: root.tagClicked(index)
                }
            }
        }
    }
}
