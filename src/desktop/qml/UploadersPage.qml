import Photokinesis 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2

ColumnLayout {
    id: root

    property var model: null
    property alias selectedIndexes: serviceView.selectedIndexes
    property bool done: selectedIndexes.length > 0

    Label {
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        font.italic: true
        text: qsTr("Select the services you'd like to upload your photos to")
    }

    ListModel {
        id: fakeModel
        ListElement {
            display: "500px"
            icon: "/home/mardy/src/git/photokinesis/plugins/500px/icon.svg"
            enabled: true
        }
        ListElement {
            display: "DeviantArt"
            icon: "/home/mardy/src/git/photokinesis/plugins/deviantart/icon.svg"
            enabled: true
        }
        ListElement {
            display: "Facebook"
            icon: "/home/mardy/src/git/photokinesis/plugins/facebook/icon.svg"
            enabled: true
        }
        ListElement {
            display: "Flickr"
            icon: "/home/mardy/src/git/photokinesis/plugins/flickr/icon.svg"
            enabled: true
        }
        ListElement {
            display: "Instagram"
            icon: "/home/mardy/src/git/photokinesis/plugins/instagram/icon.svg"
            enabled: true
        }
        ListElement {
            display: "VK"
            icon: "/home/mardy/src/git/photokinesis/plugins/vk/icon.svg"
            enabled: true
        }
    }

    ScrollView {
        Layout.fillWidth: true
        Layout.fillHeight: true

        GridView {
            id: serviceView
            property var selectedIndexes: []
            property int columns: width / cellWidth
            property int offset: (width - columns * cellWidth) / 2
            cellWidth: 240
            cellHeight: 80
            model: root.model
            delegate: UploaderDelegate {
                offsetX: serviceView.offset
                width: serviceView.cellWidth
                height: serviceView.cellHeight
                displayName: model.display
                icon: model.icon
                selected: serviceView.isSelected(index)
                onClicked: serviceView.toggleSelection(index)
            }

            function isSelected(index) {
                return selectedIndexes.indexOf(index) >= 0
            }

            function toggleSelection(index) {
                var i = selectedIndexes.indexOf(index)
                var newList = selectedIndexes
                if (i < 0) {
                    newList.push(index)
                } else {
                    newList.splice(i, 1)
                }
                newList.sort()
                selectedIndexes = newList
            }

            function clearSelection() {
                selectedIndexes = []
            }
        }
    }
}
