import ListProxyModel 0.1
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: root

    property var items: null
    property var uploaders: null

    spacing: 12

    property int _column: root.uploaders.column(0)
    property int _oldCount: 0
    property int _oldContentY: 0

    Label {
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        font.italic: true
        text: qsTr("Here you can review the upload options for each item. If you need to make changes, go back to the previous page.")
    }

    GroupBox {
        Layout.fillWidth: true
        Layout.minimumWidth: 100
        title: qsTr("Showing upload options for:")
        Flow {
            anchors.fill: parent
            Repeater {
                model: uploaders
                delegate: UploaderDelegate {
                    width: 180
                    height: 40
                    displayName: model.display
                    icon: model.icon
                    selected: index == root.uploaders.uploaderIndex(root._column)
                    onClicked: root.setColumn(root.uploaders.column(index))
                }
            }
        }
    }

    ScrollView {
        Layout.fillWidth: true
        Layout.fillHeight: true

        ListView {
            id: itemView

            spacing: 2
            model: listModel
            delegate: ItemOptionSummaryDelegate {
                width: itemView.width
                property var config: model.config
                imageUrl: model.url
                imageCaption: model.fileName
                title: config.title
                description: config.description
                tags: config.tags
            }
            onCountChanged: if (count == root._oldCount) {
                contentY = root._oldContentY
            }
        }
    }

    ListProxyModel {
        id: listModel
        sourceModel: root.items
        filterColumn: root._column
    }

    function setColumn(column) {
        root._oldContentY = itemView.contentY
        root._oldCount = itemView.count
        root._column = column
    }
}
