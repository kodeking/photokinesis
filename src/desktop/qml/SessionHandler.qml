import Photokinesis 1.0
import QtQml 2.2

Session {
    id: root

    property var uploadModel: null
    property var uploaders: null
    property bool canResume: !sessionIsFinished(_lastSessionData)

    property var _lastSessionData: null
    property var _timer: Timer {
        id: timer
        interval: 1000
        onTriggered: root.collectAndStore()
    }

    property var _conn: Connections {
        target: root.uploadModel
        onConfigChanged: timer.restart()
    }

    property var _conn2: Connections {
        target: root.uploaders
        onSelectedIndexesChanged: timer.restart()
    }

    Component.onCompleted: {
        var currentUploaders = getUploaderNames(root.uploaders.sourceModel)
        _lastSessionData = lastSession(currentUploaders)
    }

    function resume() {
        loadConfig(_lastSessionData)
        _lastSessionData = {}
    }

    function collectAndStore() {
        var data = {
            "uploadModel": root.uploadModel.config,
            "uploaders": getUploaders(root.uploaders)
        }
        storeSession(data)
    }

    function getUploaderNames(uploaders) {
        var data = []

        for (var i = 0; i < uploaders.count; i++) {
            var name = uploaders.get(i, "name")
            data.push(name)
        }

        return data
    }

    function getUploaders(selectedUploaders) {
        var data = []

        var uploaders = selectedUploaders.sourceModel
        for (var i = 0; i < uploaders.count; i++) {
            var name = uploaders.get(i, "name")
            var selected = selectedUploaders.selectedIndexes.indexOf(i) >= 0
            data.push({
                "name": name,
                "selected": selected
            })
        }

        return data
    }

    function setUploaders(data) {
        var selectedIndexes = []
        for (var i = 0; i < data.length; i++) {
            var uploaderData = data[i]
            var index = uploaderData["index"] - 1
            if (uploaders.sourceModel.get(i, "name") != uploaderData.name) {
                console.warn("Uploader index mismatch, ignoring!")
                continue
            }
            if (uploaderData.selected) {
                selectedIndexes.push(i)
            }
        }
        uploaders.selectedIndexes = selectedIndexes
    }

    function loadConfig(data) {
        root.uploadModel.config = data["uploadModel"]
        setUploaders(data["uploaders"])
    }
}
