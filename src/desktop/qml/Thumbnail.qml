import QtQuick 2.5

Image {
    id: image

    property int maxSize: 100
    sourceSize { width: maxSize; height: maxSize }
    fillMode: Image.PreserveAspectFit
    smooth: true
    asynchronous: true
    autoTransform: true
}
