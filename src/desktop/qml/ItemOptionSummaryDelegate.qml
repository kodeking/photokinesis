import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

RowLayout {
    id: root

    property alias imageUrl: image.source
    property alias imageCaption: image.caption
    property string title: ""
    property string description: ""
    property var tags: []

    ImageDelegate {
        id: image
        Layout.fillWidth: false
        width: height
        height: 128
    }

    ColumnLayout {
        Layout.fillWidth: true
        Layout.fillHeight: true
        spacing: 12

        RowLayout {
            id: titleInfo
            Layout.fillWidth: true
            visible: root.title ? true : false
            Label {
                font.bold: true
                text: qsTr("Title")
            }
            Label {
                Layout.fillWidth: true
                wrapMode: Text.Wrap
                text: root.title
            }
        }
        Label {
            Layout.fillWidth: true
            visible: !titleInfo.visible
            font { italic: true; weight: Font.Light }
            text: qsTr("no title set")
        }

        ColumnLayout {
            id: descriptionInfo
            Layout.fillWidth: true
            visible: root.description ? true : false
            Label {
                Layout.fillWidth: true
                font.bold: true
                text: qsTr("Description")
            }
            Label {
                Layout.fillWidth: true
                wrapMode: Text.Wrap
                text: root.description
            }
        }
        Label {
            visible: !descriptionInfo.visible
            font { italic: true; weight: Font.Light }
            text: qsTr("no description set")
        }

        ColumnLayout {
            id: tagsInfo
            Layout.fillWidth: true
            visible: root.tags.length > 0
            Label {
                Layout.fillWidth: true
                font.bold: true
                text: qsTr("Tags")
            }
            TagView {
                Layout.fillWidth: true
                tags: root.tags
            }
        }
        Label {
            visible: !tagsInfo.visible
            font { italic: true; weight: Font.Light }
            text: qsTr("no tags set")
        }
    }
}
