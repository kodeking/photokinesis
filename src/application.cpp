/*
 * Copyright (C) 2017-2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"

#include <QDebug>
#include <QIcon>
#include <QThreadPool>
#include <QtWebView>

using namespace Photokinesis;

namespace Photokinesis {

class ApplicationPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Application)

public:
    ApplicationPrivate(Application *q);

private:
    Application *q_ptr;
};

} // namespace

ApplicationPrivate::ApplicationPrivate(Application *q):
    QObject(),
    q_ptr(q)
{
}

Application::Application(int &argc, char **argv):
#ifdef DESKTOP_BUILD
    QApplication(argc, argv),
#else
    QGuiApplication(argc, argv),
#endif
    d_ptr(new ApplicationPrivate(this))
{
    setApplicationDisplayName(APPLICATION_DISPLAY_NAME);
    setApplicationName(APPLICATION_NAME);
    setApplicationVersion(APPLICATION_VERSION);
    setOrganizationName(QString());
    setOrganizationDomain(APPLICATION_NAME);
    setWindowIcon(QIcon(":/icons/photokinesis"));

    /* for libauthentication */
    QtWebView::initialize();
    addLibraryPath(applicationDirPath() + "/../lib");
}

Application::~Application()
{
    QThreadPool::globalInstance()->waitForDone(5000);
}

#include "application.moc"
