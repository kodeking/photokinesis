/*
 * Copyright (C) 2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_SESSION_H
#define PHOTOKINESIS_SESSION_H

#include <Photokinesis/JsonStorage>

#include <QStringList>

namespace Photokinesis {

class SessionPrivate;
class Session: public JsonStorage
{
    Q_OBJECT

public:
    Session(QObject *parent = 0);
    ~Session();

    Q_INVOKABLE void storeSession(const QJsonObject &session);
    Q_INVOKABLE QJsonObject lastSession() const;
    Q_INVOKABLE QJsonObject lastSession(const QStringList &uploaders) const;
    Q_INVOKABLE bool sessionIsFinished(const QJsonObject &session) const;

Q_SIGNALS:
    void currentUploadersChanged();

private:
    QScopedPointer<SessionPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Session)
};

} // namespace

#endif // PHOTOKINESIS_SESSION_H
