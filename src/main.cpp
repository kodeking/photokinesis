/*
 * Copyright (C) 2017-2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"

#include "executor.h"
#include "registry.h"
#include "rounded_image_provider.h"
#include "session.h"
#include "uploader_model.h"

#include <Photokinesis/AbstractUploader>
#include <Photokinesis/UploadModel>
#include <QLoggingCategory>
#include <QQmlApplicationEngine>
#include <QtQml>

using namespace Photokinesis;

int main(int argc, char *argv[])
{
    // Workaround for https://bugs.launchpad.net/bugs/1323853
    qputenv("UBUNTU_MENUPROXY", "");

    Application app(argc, argv);

    QLoggingCategory::setFilterRules(QStringLiteral("qtc.ssh=false"));

    QQmlApplicationEngine engine;
    engine.addImageProvider(QStringLiteral("rounded"),
                            new RoundedImageProvider);

    qmlRegisterType<Mardy::Registry>("Mardy", 1, 0, "Registry");

    qmlRegisterType<QAbstractListModel>();
    qmlRegisterType<Executor>("Photokinesis", 1, 0, "Executor");
    qmlRegisterType<Session>("Photokinesis", 1, 0, "Session");
    qmlRegisterType<UploadModel>("Photokinesis", 1, 0, "UploadModel");
    qmlRegisterType<UploaderModel>("Photokinesis", 1, 0, "UploaderModel");
    qmlRegisterUncreatableType<AbstractUploader>("Photokinesis", 1, 0,
                                                 "AbstractUploader",
                                                 "Can't create AbstractUploader");
    engine.addImportPath("qrc:/");
    engine.load(QUrl("qrc:/main.qml"));

    return app.exec();
}
